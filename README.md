# Roll

This is a simple command line utility to get the results of dice rolls for `{{ application.relevantUseCase }}`.

## Installation

You will need:

1. A [.NET SDK](https://dotnet.microsoft.com/en-us/download). We're targeting .NET 6, but, realistically, you could use pretty much any .NET version, Framework or otherwise.

2. A positive attitude.

### Steps

1. Clone the repository
2. Navigate to the new `roll` directory.
3. Then, depending on your platform, use one of the following commands:
    * Linux: 
        * `dotnet publish -c Release -o out --self-contained=true -r linux-x64`
    * Windows:
        * `dotnet publish -c Release -o out --self-contained=true -r win-x64`
    * MacOS/OSX:
        * `dotnet publish -c Release -o out --self-contained=true -r osx-x64`
    * Something not listed here? Check the list of [runtime identifiers](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog) for your platform of choice, and stick it after the `-r` flag.
4. Locate the executable in the `out/` directory. (`roll` on Linux or Mac, `roll.exe` on Windows)
5. Copy that somewhere in your path. 
    * `/usr/share/bin` is a good one for most Linux distros.
    * Windows has its own whole system for managing your path. Stick it somewhere sane, add it to your path.
    * Mac should be pretty similar to Linux. Go duckduckgo it or something.
6. You're done!

## Usage

To run the program, simply:

```
$ roll 2d20
Rolled [2d20]: (17) 9 8
$ roll 1d20 4d6
Rolled [1d20]: (19) 19
Rolled [4d6]: (15) 6 3 2 4
```

The format for a dice roll is as follows:`[X]d[Y]`

* `X`: The number of dice to roll. Has to be digits.
* `d`: the literal `d`. Lets us quickly parse through inputs.
* `Y`: The type of dice to roll. The program recognizes 2, 3, 4, 6, 8, 10, 12, 20 and 100 sided dice.

Practically speaking, you can roll as many dice or sets of dice as you want. We're using 64-bit unsigned integers under the hood, so you'll end up rolling over if you roll too many dice at once, but, importantly, you'll still get all of the results of any number of individual dice under 18,446,744,073,709,551,615. . . Eventually. If you have enough memory.