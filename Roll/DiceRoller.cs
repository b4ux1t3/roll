namespace Roll;

public static class DiceRoller
{
    private static readonly Random Rng = new();
    public static ulong MaximumRolls = 1000;
    public static RollResult RollDice(ulong number, int type)
    {
        return RollDice(new(number, type));
    }
    public static RollResult RollDice(DieRoll roll)
    {
        List<Tuple<int, ulong>> rolls = new();
        if (roll.Num > MaximumRolls) throw new TooManyDieRollsException();
        for (var i = 0UL; i < roll.Num; i++)
        {
            rolls.Add(new(roll.Type, (ulong) Rng.Next(roll.Type) + 1));
        }
        var result = new RollResult(rolls.Select(tuple => tuple.Item2), roll);
        return result;
    }
    public class TooManyDieRollsException: Exception {}
}
