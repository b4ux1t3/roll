using System.Text.RegularExpressions;

namespace Roll;

public record DieRoll(ulong Num, int Type)
{
    public static readonly int[] ValidDieTypes = {2, 3, 4, 6, 8, 10, 12, 20, 100};
    private const string DieRegex = @"(\d+)d(\d+)";

    public bool IsValidDie => ValidDieTypes.Contains(Type);

    public static bool TryParseDie(string input, out DieRoll? roll)
    {
        var match = Regex.Match(input, DieRegex);
        if (!match.Success)
        {
            roll = null;
            return false;
        }
        roll = new DieRoll(ulong.Parse(match.Groups[1].Captures.First().Value),
            int.Parse(match.Groups[2].Captures.First().Value));
        return roll.IsValidDie;
    }

    public static DieRoll[] FindAllDieRolls(string input)
    {
        var splitString = Regex.Split(input, @"\s+");
        DieRoll[] matches = splitString.Select(str =>
        {
            var didItWork = TryParseDie(str, out var myRoll);
            return !didItWork ? null : myRoll;
        }).Where(roll => roll is not null).ToArray()!;

        return matches;
    }
    
    public override string ToString()
    {
        return $"[{Num}d{Type}]";
    }
}
