using System.Text;

namespace Roll;

public record RollResult(IEnumerable<ulong> Rolls, DieRoll Roll)
{
    public ulong Result => Rolls.Aggregate(0UL, (sum, val) => sum + val);
    public override string ToString()
    {
        return $"Rolled {Roll}: ({Result}) {String.Join(' ', Rolls)}";
    }
}
